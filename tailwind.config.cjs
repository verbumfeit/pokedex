module.exports = {
  mode: 'jit',
  purge: ['./src/**/*.svelte','./src/**/*.html'],
  darkMode: 'media',
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
