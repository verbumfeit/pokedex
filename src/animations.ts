import { backInOut } from 'svelte/easing'

export function spin(node, { duration, easing }) {
	return {
		duration,
		css: (t) => {
			const eased = easing(t)

			return `
            transform: scale(${eased}) rotate(${eased * 720}deg);
            );`
		}
	}
}
