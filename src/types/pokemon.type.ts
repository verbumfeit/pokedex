export type PokemonType = {
	name: string
	id: number
}

export type PokemonDetailsType = {
	name: string
	id: number
	image: string
	height: number
	weight: number
	types: string[]
}
