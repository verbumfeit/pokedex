import { writable } from 'svelte/store'

export const generationStore = writable<string>()
export const editionStore = writable<string>()
